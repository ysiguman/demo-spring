package com.example.demo;

import com.example.demo.model.Hello;
import com.example.demo.model.Person;
import com.example.demo.model.repository.HelloRepository;
import com.example.demo.model.repository.PersonRepository;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class HelloControllerTests {

	@Autowired
    private HelloRepository helloRepository;
	@Autowired
    private PersonRepository personRepository;
	@Autowired
    private HelloController helloController;

	@Test
	public void testGetingHello() {
        Hello hello = new Hello();
        Person person = new Person();

        person.setName("Victor");
        person.setMail("abc@gmail.com");
        person = personRepository.save(person);

        hello.setQuote("Bonjour");
        hello.setName(person);
        hello = helloRepository.save(hello);

        Long id = hello.getId();

        Hello hello2 = helloController.helloByid(id);

        assertEquals("Victor", hello2.getName().getName());
	}

	@Test
    public void testSavingHello() {
        final String name = "Paul";
        final String quote = "salut";

        Hello hello;
        hello = helloController.create(name, quote);

        assertEquals(name, hello.getName().getName());
    }

    @Ignore
    @Test
    public void testDelete() {

    }

    @Ignore
    @Test
    public void testUpdate() {

    }

}

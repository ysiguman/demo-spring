package com.example.demo.model.repository;

import com.example.demo.model.Hello;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HelloRepository extends CrudRepository<Hello, Long> {
    List<Hello> findAll();
}

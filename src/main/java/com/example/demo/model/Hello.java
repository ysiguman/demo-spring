package com.example.demo.model;

import javax.persistence.*;

@Entity
@Table(name = "quote")
public class Hello {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "author_id")
    private Person author;

    @Column(name = "quote")
    private String quote;

    public Hello() {
    }

    public Hello(Long id, Person author, String quote) {
        this.id = id;
        this.author = author;
        this.quote = quote;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Person getName() {
        return author;
    }

    public void setName(Person name) {
        this.author = name;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }
}

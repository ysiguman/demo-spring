package com.example.demo;

import com.example.demo.model.Hello;
import com.example.demo.model.Person;
import com.example.demo.model.repository.HelloRepository;
import com.example.demo.model.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Member;
import java.util.List;

@RestController
public class HelloController {
    @Autowired
    private HelloRepository helloRepository;
    @Autowired
    private PersonRepository personRepository;


    @RequestMapping(value = "/example-all", method = RequestMethod.GET)
    public List<Hello> hello() {
        return helloRepository.findAll();
    }

    @RequestMapping(value = "/example", method = RequestMethod.GET)
    public Hello helloByid(@RequestParam Long id) {
        Hello hello = helloRepository.findOne(id);
        return hello;
    }

    @RequestMapping(value = "/create", method = RequestMethod.PUT)
    public Hello create(@RequestParam String name, @RequestParam String quote) {
        Hello hello = new Hello();
        Person author = personRepository.findByName(name);

        if (author == null) {
            author = new Person();
            author.setName(name);
            personRepository.save(author);
        }
        hello.setName(personRepository.findByName(name));

        hello.setQuote(quote);

        return helloRepository.save(hello);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public void delete(@RequestParam Long id) {
        helloRepository.delete(id);
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public Hello update(
            @RequestParam Long id,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String quote) {
        Hello hello = helloRepository.findOne(id);

        hello.setName(name == null ? hello.getName() : personRepository.findByName(name));
        hello.setQuote(quote == null ? hello.getQuote() : quote);

        return helloRepository.save(hello);
    }
}
